
# CHANGELOG

This CHANGELOG is a format conforming to [keep-a-changelog](https://github.com/olivierlacan/keep-a-changelog). 
It is generated with git-chglog -o CHANGELOG.md


<a name="v0.0.3"></a>
## [v0.0.3](https://gitlab.com/cestus/fabricator/fabricator-generate-project-go/compare/v0.0.2...v0.0.3) 

### Feat

* add support for multiple project types


<a name="v0.0.2"></a>
## [v0.0.2](https://gitlab.com/cestus/fabricator/fabricator-generate-project-go/compare/v0.0.1...v0.0.2) 

### Feat

* add support for project types

### Fix

* release version (forgotten version increment)


<a name="v0.0.1"></a>
## [v0.0.1](https://gitlab.com/cestus/fabricator/fabricator-generate-project-go/compare/v0.0.0...v0.0.1) 

### Feat

* add goreleaser


<a name="v0.0.0"></a>
## v0.0.0 

### Feat

* implement plugin
* generate plugin

